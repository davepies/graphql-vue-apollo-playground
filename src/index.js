import Vue from "vue";
import VueApollo from "vue-apollo";
import ApolloClient from "apollo-boost";
import App from "./App.vue";

const apolloClient = new ApolloClient({
  // You should use an absolute URL here
  uri: "http://localhost:8080/api",
});

const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});

Vue.use(VueApollo);

new Vue({
  apolloProvider,
  render: (createElement) => createElement(App),
}).$mount("#app");
