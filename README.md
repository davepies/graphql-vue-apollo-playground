## Simple GraphQL / Vue-apollo setup for experimentation

## Includes

* Apollo server
* Vue App with connected vue-apollo client
* Live reloading (both client and server)
* Example schema and query

## Usage

* Start both client and server: `npm start`
* Start only client (handy for testing network error handling): `npm run start:client`
* Start only server: `npm run start:server`

* client `http://localhost:8080`
* graphql-playground `http://localhost:4000`

## Pagination

A pagination example has been included. You can try it out by using the following:

Query:

```graphql
query ExampleFoo ($after:String, $before:String, $first:Int, $last:Int) {
	foo {
    bars (after:$after, before:$before, first:$first, last:$last) {
      pageInfo {
        hasNextPage
      }
      edges {
        node {
          id,
          bar
        }       
      }
    }
  }
}
```

Variables:

```json
{
  "first":1
}
```