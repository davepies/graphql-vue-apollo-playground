import { ApolloServer, gql } from "apollo-server";
// used to mock a relay-type connection for supporting pagination
// https://github.com/graphql/graphql-relay-js#connections
import { connectionFromArray } from "graphql-relay";

// mock data
import Bars from "./data/bars.json";

// The GraphQL schema
const typeDefs = gql`
  interface Node {
    id: ID!
  }

  type PageInfo {
    hasNextPage: Boolean!
    hasPreviousPage: Boolean!
    startCursor: String
    endCursor: String
  }

  type BarConnection {
    edges: [BarEdge]
    pageInfo: PageInfo!
  }

  type BarEdge {
    cursor: String!
    node: Bar
  }

  type Bar implements Node {
    id: ID!
    bar: String!
  }

  type Foo {
    bars(after: String, before: String, first: Int, last: Int): BarConnection
  }

  type Query {
    hello(delay: Int): String
    foo: Foo
  }
`;

// A map of functions which return data for the schema.
const resolvers = {
  Node: {
    __resolveType: () => "Foo",
  },
  Foo: {
    bars: (_, args) => connectionFromArray(Bars, args),
  },
  Query: {
    foo: () => ({}),
    hello: (_, { delay = 0 }) => {
      console.log(delay);
      return new Promise((resolve) => {
        setTimeout(
          () =>
            resolve(
              `world (with a ${delay}ms ${delay < 1 ? "🚀🤔" : ""} delay)`
            ),
          2 * delay
        );
      });
    },
  },
};

const server = new ApolloServer({
  typeDefs,
  resolvers,
  // Setting this to "true" will return mocked data for every field
  mockEntireSchema: false,
  mocks: true,
});

server.listen().then(({ url }) => {
  console.log(`Apollo-Server ready at ${url}`);
});
